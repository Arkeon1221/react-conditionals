console.log('App.js is running!');

// JSX - JavaScript XML 

const book = {
    title: 'Lord Of The Rings',
    subtitle: 'The Fellowship of the Ring',
    options: ['One', 'Two', 'Three']
};


const template = (
    <div>
        <h1>{book.title}</h1>
        {book.subtitle && <p>{book.subtitle}</p>}
        <p>{book.options.length > 0 ? 'Here are your options:' : 'No options...'}</p>
        <ol>
            <li>Item one</li>
            <li>Item two</li>    
        </ol>
    </div>
);

const user = {
    name: 'Andrew',
    age: 25,
    location: 'Philadelphia'

};
function getLocation(location){
    if(location) {
        return <p>Location: {location}</p>;
    } 
}

const templateTwo = (
    <div>
        <h1>{user.name ? user.name : 'Anonymous'}</h1>

        {(user.age && user.age >= 18) && <p>Age: {user.age}</p>}
        {getLocation(user.location)}
       
    </div>
);

const appRoot = document.getElementById('app')

ReactDOM.render(template, appRoot);