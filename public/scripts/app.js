'use strict';

console.log('App.js is running!');

// JSX - JavaScript XML 

var book = {
    title: 'Lord Of The Rings',
    subtitle: 'The Fellowship of the Ring',
    options: ['One', 'Two', 'Three']
};

var template = React.createElement(
    'div',
    null,
    React.createElement(
        'h1',
        null,
        book.title
    ),
    book.subtitle && React.createElement(
        'p',
        null,
        book.subtitle
    ),
    React.createElement(
        'p',
        null,
        book.options.length > 0 ? 'Here are your options:' : 'No options...'
    ),
    React.createElement(
        'ol',
        null,
        React.createElement(
            'li',
            null,
            'Item one'
        ),
        React.createElement(
            'li',
            null,
            'Item two'
        )
    )
);

var user = {
    name: 'Andrew',
    age: 25,
    location: 'Philadelphia'

};
function getLocation(location) {
    if (location) {
        return React.createElement(
            'p',
            null,
            'Location: ',
            location
        );
    }
}

var templateTwo = React.createElement(
    'div',
    null,
    React.createElement(
        'h1',
        null,
        user.name ? user.name : 'Anonymous'
    ),
    user.age && user.age >= 18 && React.createElement(
        'p',
        null,
        'Age: ',
        user.age
    ),
    getLocation(user.location)
);

var appRoot = document.getElementById('app');

ReactDOM.render(template, appRoot);
